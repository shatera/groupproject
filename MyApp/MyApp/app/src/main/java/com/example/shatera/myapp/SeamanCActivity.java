package com.example.shatera.myapp;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class SeamanCActivity extends AppCompatActivity {

     private Button playVideo;
     private TextView tv1;
     private Button playVideo2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seaman_c);

        tv1 = (TextView) findViewById(R.id.textView6);
        tv1.setText("C is a procedural programing language that was developed in the " +
                "late 1960's. Many other programing languages have based their syntax off of" +
                " the C programing language. Click on the buttons below to" +
                " view a video on C by Jeff Seaman or The New Boston.");

        playVideo = (Button) findViewById(R.id.buttonPlay);
        playVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=YCUhys772Mc")));
                Log.i("Video", "Video is Playing");
            }
        });

        playVideo2 = (Button) findViewById(R.id.buttonPlay2);
        playVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=2NWeucMKrLI&list=PL6gx4Cwl9DGAKIXv8Yr6nhGJ9Vlcjyy")));
                Log.i("Video", "Video is Playing");
            }
        });
    }

}
