package com.example.shatera.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class PracticeTestC4 extends AppCompatActivity {

    private Button submit;
    private RadioButton a1, a2,a3, a4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpracticetest4);

        a1= (RadioButton) findViewById(R.id.a1q4c);
        a2= (RadioButton) findViewById(R.id.a2q4c);
        a3= (RadioButton) findViewById(R.id.a2q4c);
        a4= (RadioButton) findViewById(R.id.a2q4c);

        submit= (Button) findViewById(R.id.btnSubmitQ1);
        submit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (a3.isChecked()) {

                    Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PracticeTestC4.this,PracticeTestC5.class);
                    startActivity(intent);

                } else
                {Toast.makeText(getApplicationContext(), "Incorrect!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PracticeTestC4.this,PracticeTestC5.class);
                    startActivity(intent);}
            }
        });


    }
}
