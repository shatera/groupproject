package com.example.shatera.myapp;

import android.content.Intent;
import android.graphics.RadialGradient;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private CheckBox Cpchk;
    private CheckBox HTMLchk;
    private CheckBox Javachk;
    private Button Submitbtn;
    //For testing only
    private Button toSeamanC;
    private Button toSeamanJava;
    private Button toSeamanIOS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerCp();
        addListenerHTML();
        addListenerJava();

<<<<<<< HEAD
=======
        //For testing purposes only
        toSeamanC = (Button) findViewById(R.id.buttontoSeamanC2);
        toSeamanC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SeamanCActivity.class);
                startActivity(intent);
            }
        });
        toSeamanJava = (Button) findViewById(R.id.buttontoSeamanJava);
        toSeamanJava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,SeamanJavaActivity.class);
                startActivity(intent);
            }
        });
        toSeamanIOS = (Button) findViewById(R.id.buttontoSeamanIOS);
        toSeamanIOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SeamanIOSActivity.class);
                startActivity(intent);
            }
        });

        Submitbtn = (Button) findViewById(R.id.btnSubmit);
        Submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(MainActivity.this,Regstration2.class);
               startActivity(intent);
              //* intent.putExtra("Message","The First Page says helllo");
               //* intent.putExtra("SecondMsg","Ahola from page 1");
               //* intent.putExtra("value",90120);

>>>>>>> 0717b0d4edd527469fff6d1f810b5dca447beaba


    }
    // this method to show a toast message when user selects C++
    public void addListenerCp(){
        Cpchk = (CheckBox) findViewById(R.id.chkCp);
        Cpchk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()){
                    Intent intent = new Intent(MainActivity.this,SeamanJavaActivity.class);
                    startActivity(intent);
                }
            }
        });// End of addListenerCp

    }
    // similar to above method but messages displays when HTML/CSS is selected
    public void addListenerHTML(){
        HTMLchk = (CheckBox) findViewById(R.id.chkHTML);
        HTMLchk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()){
                    Intent intent = new Intent(MainActivity.this,SeamanIOSActivity.class);
                    startActivity(intent);
                }
            }
        });// End of addListenerHTML
    }
    public void addListenerJava(){
        Javachk = (CheckBox) findViewById(R.id.chkJava);
        Javachk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()){}
                Intent intent = new Intent(MainActivity.this,SeamanCActivity.class);
                startActivity(intent);
            }
        });
    }
            }


