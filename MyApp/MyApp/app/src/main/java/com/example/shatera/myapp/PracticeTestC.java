package com.example.shatera.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class PracticeTestC extends AppCompatActivity {

    int answer1;
    private Button submit;
    private RadioButton a1, a2, a3, a4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpracticetest1);

        a1= (RadioButton) findViewById(R.id.a1q1C);
        a2= (RadioButton) findViewById(R.id.a2q1c);
        a3= (RadioButton) findViewById(R.id.a3q1c);
        a4= (RadioButton) findViewById(R.id.a4q1c);

        submit= (Button) findViewById(R.id.btnSubmitQ1);
        submit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (a1.isChecked()) {

                    Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(PracticeTestC.this,PracticeTestC2.class);
                    startActivity(intent);

                } else
                {Toast.makeText(getApplicationContext(), "Incorrect!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PracticeTestC.this,PracticeTestJava2.class);
                    startActivity(intent);}

            }
        });


    }
}
