package com.example.shatera.myapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SeamanJavaActivity extends AppCompatActivity {

    private Button playVideo;
    private TextView tv1;
    private Button playVideo2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seaman_java);


        tv1 = (TextView) findViewById(R.id.textView2);
        tv1.setText("Java is an object oriented programing language that is widely" +
                " used. Click on the buttons below to view a video by Jeff Seaman or The New Boston.");

        playVideo = (Button) findViewById(R.id.buttonLink4);

        playVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=G1-N7WNUExA")));
                Log.i("Video", "Video is Playing");
            }
        });

        playVideo2 = (Button) findViewById(R.id.buttonLink4);

        playVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=Hl-zzrqQoSE&list=PLFE2CE09D83EE3E28")));
                Log.i("Video", "Video is Playing");
            }
        });

            }
        }