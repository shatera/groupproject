package com.example.shatera.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class PracticeTestJava3 extends AppCompatActivity {

    private Button submit;
    private RadioButton a1,a2,a3,a4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practicetest3);

        a1= (RadioButton) findViewById(R.id.a1q3);
        a2= (RadioButton) findViewById(R.id.a2q3);
        a3= (RadioButton) findViewById(R.id.a3q3);
        a4= (RadioButton) findViewById(R.id.a4q3);

        submit= (Button) findViewById(R.id.btnSubmitQ1);
        submit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (a2.isChecked()) {

                    Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PracticeTestJava3.this,PracticeTestJava4.class);
                    startActivity(intent);

                } else
                {Toast.makeText(getApplicationContext(), "Incorrect!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PracticeTestJava3.this,PracticeTestJava4.class);
                    startActivity(intent);}
            }
        });


    }
}
