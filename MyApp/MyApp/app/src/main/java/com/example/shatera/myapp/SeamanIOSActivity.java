package com.example.shatera.myapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SeamanIOSActivity extends AppCompatActivity {

    private Button playVideo;
    private TextView tv1;
    private Button playVideo2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seaman_ios);

        tv1 = (TextView) findViewById(R.id.textView4);
        tv1.setText("iOS is Apple's mobile operating system. Apps for apple devices can me" +
                " made with Apple's xCode programing software. Click on the button below to view" +
                " a video by Jeff Seaman or The New Boston.");

        playVideo = (Button) findViewById(R.id.buttonLink2);

        playVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=lT0sGT7PxeU&list=PLugjnBl3zi6aAFbY0GGRchOovOZ4VIHOr")));
                Log.i("Video", "Video is Playing");

            }
        });
                playVideo2 = (Button) findViewById(R.id.buttonLink3);

                playVideo2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=abcMmyhKCno&list=PL53038489615793F7")));
                        Log.i("Video", "Video is Playing");
            }
        });

            }

    }