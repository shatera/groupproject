package com.example.shatera.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Regstration2 extends AppCompatActivity {
    private Button prevbtn;
    private Button nextbtn;
    private TextView Showtxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regstration2);
        Bundle myExtra = getIntent().getExtras();
        Showtxt=(TextView)findViewById(R.id.txtWelcome2);

        if(myExtra!=null){
            String mymessage = myExtra.getString("message");
            Showtxt.setText(mymessage);
        }


        prevbtn=(Button)findViewById(R.id.btnPrev);
        prevbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Regstration2.this,MainActivity.class));
            }
        });
        /* this will be the button to goto the next page
        nextbtn=(Button)findViewById(R.id.btnNext);
        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent());
            }
        });
        */
    }
}
